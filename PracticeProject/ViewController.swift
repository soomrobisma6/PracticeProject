//
//  ViewController.swift
//  PracticeProject
//
//  Created by Bisma on 19/08/2023.
//

import UIKit

class ViewController: UIViewController {
   

    override func viewDidLoad() {
        super.viewDidLoad()

//        var obj = Person(name: "Ali")
//        var obj2 = Animal(name: "Cat")
//
//        print(obj2.name)
//
//        obj2.demo()
//        obj.cat = "Cat1"
//        print(obj.cat)
        
        
        
//        Ehsan
        var obj = LazyDemo(firstName: "Muhammad", lastName: "Ali")
        print(obj.name)
    }


}

class LazyDemo{
    var firstName : String
    var lastName: String
   lazy var name = "\(firstName)  \(lastName)"
 
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    
}







class Person: FinalDemo{ // shallow copy
    var name : String?
    init(name: String? = nil) {
        self.name = name
    }
  
    override func name2() {
        cat = "Hello"
    }
    
}

struct Animal { // deep copy
    var name: String?
    mutating func demo() { //mutating used for updating the property value within struct
       name = "Ali"
        print(name)
    }
    
    
}

protocol Demo{
    func demo()
}

// MARK: - Agenda
// -> struct vs classes
// -> keywords: self vs Self, final, lazy
// -> access modifiers: public, private, fileprivate, open, internal

// private -> accessible in same class and extension within same file -> private is not accessabile in extension which is in different file
// filePrivate -> accessible in different class and extension within same file -> filePrivate is not accessabile in extension which is in different file

 class FinalDemo{
     var cat = "Cat"
  final  func name1(){ // final keyword is used to block inhertance and final method can not allow methods to be overriden
        
    }
   
    func name2(){
        
    }
    
}

public class AccessModifiers{
    public var name1 = "name1"
    private var name2 = "name2"
    fileprivate var name3 = "name3"
    internal var name4 = "name4"
    open var name5 = "name5"
}

extension AccessModifiers{
    func demo(){
        print(name1)
        print(name2)
        print(name3)
    }
}

extension AccessModifiers{
    func demo2(){
        
    }
}


